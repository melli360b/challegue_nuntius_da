class Product
  attr_accessor :name ,:type_product ,:price ,:cant ,:imported
  def initialize(name, type_product, price, cant, imported)
    @name = name
    @type_product = type_product
    @price = price
    @cant = cant
    @imported = imported
  end

  def get_result
    if @type_product == 'n'
      @price_1 = (@price.to_f*0.1).round(2)
    else
      @price_1 = 0
    end
    if @imported == 'y'
      @price_2 = (@price.to_f*0.05).round(1)
    else
      @price_2 = 0
    end
    @price = ((@price.to_f + @price_1.to_f + @price_2.to_f)*@cant.to_i).round(2)
    puts "#{@cant} #{@name}: #{@price}"
  end
end

# Enter the full names of the goods
run = 'y'
list = []
product_new = nil
while run == 'y'
  puts 'Is the product you are going to enter in the category of books, food or medical products?(y,n)'
  type_product = gets.chomp
  puts "Enter product name"
  name = gets.chomp
  puts "Is the product imported? (y,n)"
  imported = gets.chomp
  puts "Enter price"
  price = gets.to_f
  puts "Enter amount"
  cant = gets.to_i
  product_new = Product.new(name, type_product, price, cant, imported)
  list << product_new
  puts "Do you want to continue entering products?"
  run = gets.chomp
end

sales_taxes = 0
total = 0

list.each do |product_new|
  if product_new.type_product == 'n'
    price_1 = (product_new.price.to_f*0.1).round(2)
  else
    price_1 = 0
  end
  if imported == 'y'
    price_2 = (product_new.price.to_f*0.05).round(1)
  else
    price_2 = 0
  end
  product_new.get_result
  sales_taxes = sales_taxes + price_1 + price_2
  total = total + product_new.price
end
puts "Sales Texas: #{sales_taxes}"
puts "Total: #{total}"